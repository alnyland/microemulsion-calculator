function [ furtherRetain ] = selectPhase(typeset, needle)
%SELECTPHASE Find points that are of a certain criteria
%   Returns a set of all of the points that are of a certain type


    i = 1;
    for j = 1:numel(typeset)
        if (typeset(j) == needle)
            furtherRetain(i) = j;
            i = i + 1;
        end
    end
    if (i <= 1)
        fprintf("selectPhase error: Insufficient points found for type %d.\n", needle);
        furtherRetain = 0;
    end
end

