function [ txt ] = displayCoordinates( ~, info, phases )
%DISPLAYCOORDINATES Summary of this function goes here
%   Detailed explanation goes here

    maxheight = sind(60); % Entire plot is normalized
    x = info.Position(1);
    y = info.Position(2);
    s = y/sind(60);
    w = 1 - ((x) + cosd(60) * y / maxheight);
    o = 1 - w - s;

    currentPhase = phases(info.DataIndex);
    txt = sprintf("SOW: (%2d, %2d, %2d) | %s\n", round(s*100), round(o*100), round(w*100), currentPhase);

end
