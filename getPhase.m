function [ phase, color ] = getPhase( typeNum )
%GETPHASE Determines color and string (name) of phase type
%   Detailed explanation goes here

color = [0 1 0];
phase = "";

switch(typeNum)
    case 5
        color = [0 1 1];
        phase = "Type I";
    case 4
        color = [1 0 1];
        phase = "Type II";
    case 3
        color = [0 0 1];
        phase = "Type III";
    case 2
        color = [1 1 0];
        phase = "1-Phase";
    case 1
        color = [0.75 0.75 0.75];
        phase = "1-Phase";
end
end

