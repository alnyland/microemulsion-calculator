function [ txt ] = displayCoordinates3( ~, info, phases )
%DISPLAYCOORDINATES3 Summary of this function goes here
%   Detailed explanation goes here

    maxheight = sind(60); % Entire plot is normalized
    z = info.Position(1); % x in plot
    x = info.Position(2); % y in plot
    y = info.Position(3); % z in plot
    s = y/sind(60);
    o = 1 - ((x) + cosd(60) * y / maxheight);
    w = 1 - o - s;
    fourth = z;

    currentPhase = phases(info.DataIndex);
    txt = sprintf("SOW: (%2d, %2d, %2d) @ %2d | %s\n", round(s*100), round(o*100), round(w*100), round(fourth), currentPhase);
end
