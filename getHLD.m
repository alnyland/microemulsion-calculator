function [ answer ] = getHLD( ionic, S, MWt, EACN, T, Cc )
%HLD Summary of this function goes here
%   Detailed explanation goes here

    AVOGADRO = 6.0220e+23;
    saltweight = 35.45 + 22.99;
    surfactant = 3;
    K = 0.17;
    alphaIonic = 0.01;
    alphaEO = -0.06;
    C = surfactant * 10 / MWt;
    DeltaT = T - 25;

    if (ionic)
        Sal = log(S + 0.3 * saltweight / MWt * surfactant / 5);
        alpha = alphaIonic;
    else
        Sal = 0.13 * S;
        alpha = alphaEO;
    end
    answer = Sal - K * EACN - alpha * DeltaT + Cc;
end

