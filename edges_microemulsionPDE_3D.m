% Andrew Nyland - 5/3/21
% Microemulsion PDE replica
%   From https://www.stevenabbott.co.uk/practical-surfactants/tps.php
% 
% ----------------------------------
% 3D stuff works as select slices of the prism.
% Next, need to add edge finding/rendering

function edges_microemulsionPDE_3D()
    
    % For 3D render
    tempMin = 15;
    tempMax = 45;
    tempGranularity = 5;
    tempStep = (tempMax - tempMin) / tempGranularity;
    
    limitSurf = 0;
    limitWater= 0;
    limitOil  = 0;
    limitX    = 10;

    typeToFind = 3;
    gui_create;
    
    function gui_create()
        % Values
            % views
        visualmargin = 0.2; 
        axis equal;
        guiHeight = 350;
        guiWidth = 500;
        sliderTitleStart = 0;
        sliderTextStart = 300;
        sliderStart = 100;
        
        [ MWt, Area, S, xi, L, EACN, Cc, T, ionic ] = importMicroemulsionConfig('configTesting.txt');
        
        HLD = getHLD(ionic, S, MWt, EACN, T, Cc); % To populate the text field on the GUI
        updateHLDText();
        granularity = 100; % How many edges to a side of the triangle
        
        ctrl = figure('Name', 'Controls', 'position', [0, 0, guiWidth, guiHeight]);
        titleText = uicontrol(ctrl, 'Style', 'text', 'position', [0, guiHeight-100, guiWidth-100, 100], 'String', 'HLD-NAC Calculator',...
            'FontSize', 24, 'FontWeight', 'bold');
        hldText = uicontrol(ctrl, 'Style', 'text', 'position', [guiWidth-120, guiHeight-100, 50, 90], 'String', 'HLD:', 'FontWeight', 'bold');
        hldValue = uicontrol(ctrl, 'Style', 'text', 'position', [guiWidth-70, guiHeight-100, 50, 90], 'String', 'NaN');
        %ionicText = uicheckbox(ctrl, 'Text', 'Ionic', 'Position', [20, guiHeight-150, 100, 80]);
        
        viewControlsTitle = uicontrol(ctrl, 'Style', 'text', 'position', [0, guiHeight-60, guiWidth, 24], 'String', 'View Controls', 'FontSize', 20);
        granTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-100, 100, 20], 'String', 'Granularity:');
        granText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-100, 50, 20], 'String', granularity);
        granSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-100, 200, 20], 'Callback', @updateGranText,...
            'min', 10, 'max', 500, 'value', granularity);
        
        tempGranTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-120, 100, 20], 'String', 'Temp Granularity:');
        tempGranText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-120, 50, 20], 'String', tempGranularity);
        tempGranSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-120, 200, 20], 'Callback', @updateTempGranText,...
            'min', 1, 'max', 100, 'value', tempGranularity);
        
        hldnacControls = uicontrol(ctrl, 'Style', 'text', 'position', [0, guiHeight-150, guiWidth, 24], 'String', 'HLD-NAC Controls', 'FontSize', 20);
        
        % Cc slider
        ccTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-180, 100, 20], 'String', 'Cc:');
        ccText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-180, 100, 20], 'String', Cc);
        ccSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-180, 200, 20], 'Callback', @updateCcText,...
            'min', -3, 'max', 3, 'value', Cc);
        
        % EACN slider
        eacnTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-200, 100, 20], 'String', 'EACN:');
        eacnText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-200, 100, 20], 'String', EACN);
        eacnSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-200, 200, 20], 'Callback', @updateEACNText,...
            'min', -4, 'max', 30, 'value', EACN);
        
        % MWt slider
        mwtTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-220, 100, 20], 'String', 'MWt:');
        mwtText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-220, 100, 20], 'String', MWt);
        mwtSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-220, 200, 20], 'Callback', @updateMWtText,...
            'min', 100, 'max', 1000, 'value', MWt);
        
        % Area slider
        areaTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-240, 100, 20], 'String', 'Area:');
        areaText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-240, 100, 20], 'String', Area);
        areaSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-240, 200, 20], 'Callback', @updateAreaText,...
            'min', 20, 'max', 100, 'value', Area);
        
        % Length slider
        lengthTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-260, 100, 20], 'String', 'Length:');
        lengthText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-260, 100, 20], 'String', L);
        lengthSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-260, 200, 20], 'Callback', @updateLengthText,...
            'min', 5, 'max', 30, 'value', L);
        
        % Xi slider
        xiTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-280, 100, 20], 'String', 'Xi:');
        xiText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-280, 100, 20], 'String', xi);
        xiSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-280, 200, 20], 'Callback', @updateXiText,...
            'min', 10, 'max', 100, 'value', xi);
        
        % Type to select (radio)
        selectTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-330, 100, 20], 'String', 'Select type');
        selectRadio(1) = uicontrol(ctrl, 'Style', 'radio', 'position', [sliderStart, guiHeight-330, 200, 20], 'String', 1, 'Callback', @updateSelect);
        selectRadio(2) = uicontrol(ctrl, 'Style', 'radio', 'position', [sliderStart + 30, guiHeight-330, 200, 20], 'String', 2, 'Callback', @updateSelect);
        selectRadio(3) = uicontrol(ctrl, 'Style', 'radio', 'position', [sliderStart + 60, guiHeight-330, 200, 20], 'String', 3, 'Callback', @updateSelect);
        selectRadio(4) = uicontrol(ctrl, 'Style', 'radio', 'position', [sliderStart + 90, guiHeight-330, 200, 20], 'String', 4, 'Callback', @updateSelect);
        selectRadio(5) = uicontrol(ctrl, 'Style', 'radio', 'position', [sliderStart + 120, guiHeight-330, 200, 20], 'String', 5, 'Callback', @updateSelect);

        plot_stuff();
        
        function plot_stuff()
            [x, y, z, colors, hld, phases] = simulate(granularity, ionic, S, MWt, Area, xi, L, EACN, T, Cc);
            
            figure(1);
            s = scatter3(x, y, z, [], colors, 'Filled');
            
            xlim([tempMin-1, tempMax+1]);
            ylim([-1*visualmargin, visualmargin+1]);
            zlim([-1*visualmargin, visualmargin+1]);
            
            dcm = datacursormode;
            dcm.Enable = 'on';
            dcm.DisplayStyle = 'window';
            dcm.UpdateFcn = {@displayCoordinates3, phases};
            set(gcf, 'Name', 'View HLD-NAC');
            
            HLD = hld;
        end
        
        
        function updateHLDText()
            hldValue.String = num2str(HLD);
        end
        
        function updateGranText(h, ~)
            val = round(get(h, 'Value'));
            granularity = val;
            set(granText, 'String', num2str(val));
            plot_stuff();
            updateHLDText();
        end
        function updateTempGranText(h, ~)
            val = round(h.Value);
            tempGranularity = val;
            tempStep = (tempMax - tempMin) / tempGranularity;
            tempGranText.String = num2str(val);
            plot_stuff();
        end
        
        function updateCcText(h, ~)
            val = get(h, 'Value');
            Cc = val;
            ccText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateEACNText(h, ~)
            val = get(h, 'Value');
            EACN = val;
            eacnText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateMWtText(h, ~)
            val = get(h, 'Value');
            MWt = val;
            mwtText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateAreaText(h, ~)
            val = get(h, 'Value');
            Area = val;
            areaText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateLengthText(h, ~)
            val = get(h, 'Value');
            L = val;
            lengthText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateXiText(h, ~)
            val = get(h, 'Value');
            xi = val;
            xiText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateLimSText(h, ~)
            val = h.Value;
            limitSurf = round(val);
            limSText.String = num2str(limitSurf);
            plot_stuff();
        end
        function updateLimOText(h, ~)
            val = h.Value;
            limitOil = round(val);
            limOText.String = num2str(limitOil);
            plot_stuff();
        end
        function updateLimWText(h, ~)
            val = h.Value;
            limitWater = round(val);
            limWText.String = num2str(limitWater);
            plot_stuff();
        end
        function updateLimXText(h, ~)
            val = h.Value;
            limitX = round(val);
            limXText.String = num2str(limitX);
            plot_stuff();
        end
        function updateSelect(h, ~)
            val = str2num(h.String);
            typeToFind = val;
            for i = 1:numel(selectRadio)
                checkin = selectRadio(i);
                if (str2num(checkin.String) == typeToFind)
                    selectRadio(i).Value = 1;
                else
                    selectRadio(i).Value = 0;
                end
            end
            plot_stuff();
        end
            
    end
    
    function [x, y, z, colors, HLD, phases] = simulate(granularity, ionic, S, MWt, Area, xi, L, EACN, T, Cc)
        % Configuration
        breadth = granularity + 1;
        step = 1/granularity;
        ystep = sind(60)*step;
        maxheight = sind(60); % Entire plot is normalized
        
        % Data for simulation
        % phase list set
        phases = strings(1, sum(1:breadth) * (tempMax-tempMin));
        x = [];
        y = [];
        z = [];
        colors = [];
        retained = [];

        tempStep = (tempMax - tempMin) / tempGranularity;
        
        i = 1;  % Iterator
        xoffset = 0;
        typesetfinal = [];
        lastoffset = 1;
        for xx = tempMin:tempStep:tempMax
            islice = 1;
            typeset = [];
            for zz = granularity:-1:0
                xoffset = (granularity - zz) * cosd(60);
                for yy = 0:zz
                    % to disregard the lower corner points of 1phase
                    if (zz == granularity && yy == 0 || zz == granularity && yy == zz)
                        continue;
                    end
                    yc = yy * step + xoffset / granularity;
                    zc = (granularity - zz) * ystep;

                    [s, o, w] = toSOW(yc, zc);

                    xc = xx;

                    xslice(islice) = xc;
                    yslice(islice) = yc;
                    zslice(islice) = zc;

                    % HLD-NAC
                    HLD = getHLD(ionic, S, MWt, EACN, xx, Cc);
                    type = nac(s, o, w, HLD, xi, L);
                    
                    [phaseslice(islice), colorslice(islice, :)] = getPhase(type);
                    typeset(islice) = type;
                    i = i + 1;
                    islice = islice + 1;
                end
            end
            % Select points that are of the chosen type
            retain = selectPhase(typeset, typeToFind);
            if (numel(retain) < 2)
                continue;
            end
            xslice = xslice(retain);
            yslice = yslice(retain);
            zslice = zslice(retain);
            phaseslice = phaseslice(retain);
            colorslice = colorslice(retain, :);
            typeset = typeset(retain);
            % Select points from convex hull
            retain = boundary(yslice', zslice');
            xslice = xslice(retain);
            yslice = yslice(retain);
            zslice = zslice(retain);
            phaseslice = phaseslice(retain);
            colorslice = colorslice(retain, :);
            typeset = typeset(retain);
            % Append to returned vectors
            insert = lastoffset:(lastoffset+numel(retain)-1);
            x(insert) = xslice;
            y(insert) = yslice;
            z(insert) = zslice;
            colors(insert, :) = colorslice;
            phases(insert) = phaseslice;
            retained(insert) = retain;
            typesetfinal(insert) = typeset;
            lastoffset = lastoffset + numel(retain);
        end
    end % end simulate()

end