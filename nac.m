function [ answer ] = nac( Vs, Vo, Vw, HLD, xi, L )
    %NAC Net Average Curvature Calculation + Flash Algorithm
    %   Detailed explanation goes here

    % Concentration in decimal, not percent (0.XX not XX%)
    % Arg key:          FLCs, FLCo, FLCw, etaDm=xi/L
    % body
    % Hardcoded constants
    % EOSParam
    I_c = 0.5;
    H1_c = -20;
    H2_c = -0.05;

    TypeI = -2; % -2=undefined, -1=II-, 0=III, 1=II+, 2=Critical, 3=singlep

    etaDm = xi / L;
    HLDU = 2 / etaDm;

    sw0 = Vw / Vs;
    so0 = (1 - Vw - Vs) / Vs;
    CSEP = Vs / (1 - Vw);
    CSEN = Vs / (Vw + Vs);

    if (abs(HLD - HLDU) <= 1e-16)
        CSE3PP = -1e+24;
    else
        SO3P = 2 / (3 * I_c * (HLDU - HLD)) - 0.5;
        if (SO3P > -1 && SO3P < 0)
            CSE3PP = -1e+24;
        else
            CSE3PP = 1 / (1 + SO3P);
        end
    end

    if (abs(-HLD - HLDU) <= 1e-16)
        CSE3PN = 0;
    else
        SW3P = 2 / (3 * I_c * (HLDU + HLD)) - 0.5;
        if (SW3P > -1 && SW3P < 0)
            CSE3PN = -1e+24;
        else
            CSE3PN = 1 / (1 + SW3P);
        end
    end

    if (CSEN <= CSE3PN && CSEP <= CSE3PP && abs(HLD) - HLDU < 0)
        TypeI = 0; %Type III
        nph = 3;
        Vsm = 1 / (1 + SW3P + SO3P);
        Vwm = SW3P * Vsm;
        Sm = Vs / Vsm;
        Sw = Vw - Vwm * Sm;
    else
        FLA1Hs = H1_c;
        % Calculate slopes of critical tie lines
        if (HLD > -HLDU)
            etaDcP = 1 / (FLA1Hs * (exp(H2_c * (HLD + HLDU)) - 1) + 1 / etaDm);
            CEcrP = 1 / (1 + etaDcP / (6 * I_c) - 0.5);
        else
            CEcrP = 2; % A non physical value
        end

        if (HLD < HLDU)
            etaDcN = 1 / (FLA1Hs * (exp(H2_c * (-HLD + HLDU)) - 1) + 1 / etaDm);
            CEcrN = 1 / (1 + etaDcN / (6 * I_c) - 0.5);
        else
            CEcrN = 2; % A non physical value
        end

        Sm = 2; % Check for supercritical region
        EtaDP = 1;
        EtaDN = 1;

        if (CSEP >= CSE3PP && HLD > -HLDU && CSEP < CEcrP) % Type II+
            if (HLD < HLDU)
                nph = 4;
            else
                nph = 2;
            end

            BCoef = (1 / etaDcP - 1 / etaDm) / (CEcrP - CSE3PP);
            EtaDP  = 1 / (1 / etaDcP - BCoef * (CEcrP - CSEP));
            if ((EtaDP / (6 * I_c) - so0 - 0.5) > 1e-16)
                Vsm = Vs;
                Sm = 1;
            else
                Vsm = 1 / (1 + so0 + 1 / (6 * I_c / EtaDP - 1 / (so0 + 0.5)) - 0.5);
                Sm = Vs / Vsm;
            end
            Sw = 1 - Sm;
            Vwm = (Vw - Sw) / Sm;
        end

        if (Sm > 1 && HLD < HLDU && CSEN >= CSE3PN && CSEN < CEcrN) % Type II-
            if (HLD > -HLDU)
                nph = 5;
            else
                nph = 2;
            end
            BCoef = (1 / etaDcN - 1 / etaDm) / (CEcrN - CSE3PN);
            EtaDN = 1 / (1 / etaDcN - BCoef * (CEcrN - CSEN));
            if ((EtaDN / (6 * I_c) - sw0 - 0.5) > 1e-16)
                Vsm = Vs;
                Sm = 1;
            else
                Vsm = 1 / (1 + sw0 + 1 / (6 * I_c / EtaDN - 1 / (sw0 + 0.5)) - 0.5);
                Sm = Vs / Vsm;
            end

            Sw = 0;
            Vwm = Vw / Sm;
        end

        if (Sm >= 1) % single phase
            Sm = 1;
            Vsm = Vs;
            Vwm = Vw;
            Sw = 0;
            nph = 1;
        end
    end

    answer = nph;
%     Ss
%     So
%     Sw 

end

