function [ MWt, Area, S, xi, L, EACN, Cc, T, ionic ] = importMicroemulsionConfig( filename )
%IMPORTMICROEMULSIONCONFIG Summary of this function goes here
%   0 Detailed explanation goes here
    config = getMicroemulsionConfig( filename );
    MWt = config('MWt');
    Area = config('Area');
    S = config('S');
    xi = config('xi');
    L = config('L');
    EACN = config('EACN');
    T = config('T');
    Cc = config('Cc');
    ionic = config('ionic');
end

function [ map ] = getMicroemulsionConfig( filename )
% Gets the config from a text file and mutates it into a dictionary

    if (~isfile('configDefault.txt'))
        fprintf("Default config file does not exist. Looking for 'configDefault.txt' in current directory.\n");
        fprintf("Exiting\n");
        exit;
    end

    % Default configuration is loaded regardless
    defaultData = importdata('configDefault.txt');
    map = containers.Map(defaultData.textdata, defaultData.data);

    if (isfile(filename))
        % Custom configuration replaces default when defined
        data = importdata(filename);
        for i = 1:numel(data.textdata)
            map(data.textdata{i}) = data.data(i);
        end
    end
end


function exists = isfile(fn)
    exists = exist(fn, 'file') == 2;
end