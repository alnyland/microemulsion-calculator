function [ s, o, w ] = toSOW( x, y )
%TOSOW X, Y -> Surfactant/Oil/Water Concentration
%   Ternary coordinate change
%   assumes X and Y are normalized

    maxheight = sind(60); % Entire plot is normalized
    s = y/sind(60);
    w = 1 - ((1 - x) + cosd(60) * y / maxheight);
    o = 1 - w - s;

end

