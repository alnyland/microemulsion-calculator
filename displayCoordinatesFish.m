function [ txt ] = displayCoordinatesFish( ~, info, phases, xtitle, ytitle )
%DISPLAYCOORDINATESFISH Summary of this function goes here
%   Detailed explanation goes here

    maxheight = sind(60); % Entire plot is normalized
    x = info.Position(1);
    y = info.Position(2);
    
    currentPhase = phases(info.DataIndex);
    txt = sprintf("(%s:%3.1f, %s:%3.1f) = %s\n", xtitle, x, ytitle, y, currentPhase);

end
