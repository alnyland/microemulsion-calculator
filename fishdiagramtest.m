[ MWt, Area, S, xi, L, EACN, Cc, T, ionic ] = importMicroemulsionConfig('configTesting.txt');
    
% Configuration
xGranularity = 100;
yGranularity = 100;
xMin = 0;
xMax = 100;
yMin = 15; % Temp for now
yMax = 60;

x = [];
y = [];
colors = [];
phases = [];

oil = 0.5;
water = 0.5;

i = 1; % iterator
for yy = yMin:((yMax-yMin)/yGranularity):yMax
    for xx = xMin:((xMax-xMin)/xGranularity):xMax
        surfVol = xx/100;
        temp = yy;
        oil = (1 - surfVol) / 2;
        water = oil;
        hld = getHLD(ionic, S, MWt, EACN, temp, Cc);
        type = nac(surfVol, oil, water, hld, xi, L);
        x(i) = xx;
        y(i) = yy;
        
        [phases(i), colors(i, :)] = getPhase(type);
        i = i + 1;
    end
end

figure(1);
scatter(x, y, [], colors, 'Filled');