% Andrew Nyland - 5/3/21
% Microemulsion PDE replica
%   From https://www.stevenabbott.co.uk/practical-surfactants/tps.php
% Now refactored some for easier code readability/reusage
% And maybe for some discovery capabilities?

function sliders_microemulsionPDE()
    gui_create;
    
    function [x, y, colors, HLD, phases] = simulate(granularity, ionic, S, MWt, Area, xi, L, EACN, T, Cc)
        % Configuration
        breadth = granularity + 1;
        step = 1/granularity;
        ystep = sind(60)*step;
        maxheight = sind(60); % Entire plot is normalized
        
        % Data for simulation
        % phase list set
        phases = strings(1, sum(1:breadth));
        x = zeros(1, sum(1:breadth));
        y = zeros(size(x));
        colors = [];

        i = 1;  % Iterator
        xoffset = 0;
        % HLD calculation
        HLD = getHLD(ionic, S, MWt, EACN, T, Cc);
        for yi = granularity:-1:0
            xoffset = (granularity - yi) * cosd(60);
            for xj = 0:yi
                x(i) = xj * step + xoffset / granularity;
                y(i) = (granularity - yi) * ystep;
                
                [s, o, w] = toSOW(x(i), y(i));
                
                % HLD-NAC
                type = nac(s, o, w, HLD, xi, L);
                
                [phases(i), colors(i, :)] = getPhase(type);
                i = i + 1;
            end
        end
    end % end simulate()

    function gui_create()
        % views
        visualmargin = 0.2; 
        axis equal;
        guiHeight = 350;
        guiWidth = 500;
        sliderTitleStart = 0;
        sliderTextStart = 300;
        sliderStart = 100;
        granularity = 100; % How many points to a side of the triangle
        
        [ MWt, Area, S, xi, L, EACN, Cc, T, ionic ] = importMicroemulsionConfig('configTesting.txt');
        
        HLD = getHLD(ionic, S, MWt, EACN, T, Cc); % To populate the text field on the GUI
        
        ctrl = figure('Name', 'Controls', 'position', [0, 0, guiWidth, guiHeight]);
        titleText = uicontrol(ctrl, 'Style', 'text', 'position', [0, guiHeight-100, guiWidth-100, 100], 'String', 'HLD-NAC Calculator Thing',...
            'FontSize', 24, 'FontWeight', 'bold');
        hldText = uicontrol(ctrl, 'Style', 'text', 'position', [guiWidth-120, guiHeight-100, 50, 90], 'String', 'HLD:', 'FontWeight', 'bold');
        hldValue = uicontrol(ctrl, 'Style', 'text', 'position', [guiWidth-70, guiHeight-100, 50, 90], 'String', num2str(HLD));
        %ionicText = uicheckbox(ctrl, 'Text', 'Ionic', 'Position', [20, guiHeight-150, 100, 80]);
        
        viewControlsTitle = uicontrol(ctrl, 'Style', 'text', 'position', [0, guiHeight-60, guiWidth, 24], 'String', 'View Controls', 'FontSize', 20);
        granText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-100, 50, 20], 'String', granularity);
        granSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-100, 200, 20], 'Callback', @updateGranText,...
            'min', 10, 'max', 500, 'value', granularity);
        
        hldnacControls = uicontrol(ctrl, 'Style', 'text', 'position', [0, guiHeight-150, guiWidth, 24], 'String', 'HLD-NAC Controls', 'FontSize', 20);
        
        % Cc slider
        ccTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-180, 100, 20], 'String', 'Cc:');
        ccText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-180, 100, 20], 'String', Cc);
        ccSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-180, 200, 20], 'Callback', @updateCcText,...
            'min', -3, 'max', 3, 'value', Cc);
        
        % EACN slider
        eacnTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-200, 100, 20], 'String', 'EACN:');
        eacnText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-200, 100, 20], 'String', EACN);
        eacnSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-200, 200, 20], 'Callback', @updateEACNText,...
            'min', -4, 'max', 30, 'value', EACN);
        
        % MWt slider
        mwtTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-220, 100, 20], 'String', 'MWt:');
        mwtText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-220, 100, 20], 'String', MWt);
        mwtSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-220, 200, 20], 'Callback', @updateMWtText,...
            'min', 100, 'max', 1000, 'value', MWt);
        
        % Area slider
        areaTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-240, 100, 20], 'String', 'Area:');
        areaText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-240, 100, 20], 'String', Area);
        areaSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-240, 200, 20], 'Callback', @updateAreaText,...
            'min', 20, 'max', 100, 'value', Area);
        
        % Length slider
        lengthTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-260, 100, 20], 'String', 'Length:');
        lengthText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-260, 100, 20], 'String', L);
        lengthSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-260, 200, 20], 'Callback', @updateLengthText,...
            'min', 5, 'max', 30, 'value', L);
        
        % Xi slider
        xiTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-280, 100, 20], 'String', 'Xi:');
        xiText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-280, 100, 20], 'String', xi);
        xiSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-280, 200, 20], 'Callback', @updateXiText,...
            'min', 10, 'max', 100, 'value', xi);
        
        % Temperature slider
        tempTitle = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTitleStart, guiHeight-300, 100, 20], 'String', 'Temp:');
        tempText = uicontrol(ctrl, 'Style', 'text', 'position', [sliderTextStart, guiHeight-300, 100, 20], 'String', T);
        tempSlider = uicontrol(ctrl, 'Style', 'slider', 'position', [sliderStart, guiHeight-300, 200, 20], 'Callback', @updateTempText,...
            'min', 0, 'max', 90, 'value', T);
        
        plot_stuff();
        
        function plot_stuff()
            [x, y, colors, hld, phases] = simulate(granularity, ionic, S, MWt, Area, xi, L, EACN, T, Cc);
            
            % drawn = figure('Name', 'View HLD-NAC');
            figure(1);
            scatter(x, y, [], colors, 'Filled');
            surfLabel = text(0.1, 0.35, 'Surfactant %');
            surfLabel.Rotation = 60;
            oilLabel = text(.8, 0.5, 'Oil %');
            oilLabel.Rotation = -60;
            waterLabel = text(0.45, -0.1, 'Water %');
            xlim([-1*visualmargin, visualmargin+1]);
            ylim([-1*visualmargin, visualmargin+1]);
            
            dcm = datacursormode;
            dcm.Enable = 'on';
            dcm.DisplayStyle = 'window';
            dcm.UpdateFcn = {@displayCoordinates, phases};
            set(gcf, 'Name', 'View HLD-NAC');
            
            HLD = hld;
        end
        
        % Functions to update text field values when the slider is changed
        function updateHLDText()
            hldValue.String = num2str(HLD);
        end
        
        function updateGranText(h, ~)
            val = round(h.Value);
            granularity = val;
            set(granText, 'String', num2str(val));
            plot_stuff();
        end
        
        function updateCcText(h, ~)
            val = h.Value;
            Cc = val;
            ccText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateEACNText(h, ~)
            val = h.Value;
            EACN = val;
            eacnText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateMWtText(h, ~)
            val = h.Value;
            MWt = val;
            mwtText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateAreaText(h, ~)
            val = h.Value;
            Area = val;
            areaText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateLengthText(h, ~)
            val = h.Value;
            L = val;
            lengthText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateXiText(h, ~)
            val = h.Value;
            xi = val;
            xiText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
        function updateTempText(h, ~)
            val = h.Value;
            T = val;
            tempText.String = num2str(val);
            plot_stuff();
            updateHLDText();
        end
    end
    
end