% Andrew Nyland, 7/14/21
% Attempting to create a 3D surface, this is an initial trial for rendering
% HLD-NAC volumes as surfaces in matlab
clear; clc;

    typeToFind = 1;

    % For 3D render
    tempMin = 15;
    tempMax = 45;
    tempGranularity = 5;
    tempStep = (tempMax - tempMin) / tempGranularity;

    granularity = 100;
    breadth = granularity + 1;
    step = 1/granularity;
    ystep = sind(60)*step;
    maxheight = sind(60); % Entire plot is normalized

    % Data for simulation
    % phase list set
    phases = strings(1, sum(1:breadth) * (tempMax-tempMin));
    x = [];
    y = [];
    z = [];
    colors = [];
    retained = [];
    
    [ MWt, Area, S, xi, L, EACN, Cc, T, ionic ] = importMicroemulsionConfig('configTesting.txt');

    i = 1;  % Iterator
    xoffset = 0;       
    typesetfinal = [];
    lastoffset = 1;
for xx = tempMin:tempStep:tempMax
    islice = 1;
    typeset = [];
    for zz = granularity:-1:0
        xoffset = (granularity - zz) * cosd(60);
        for yy = 0:zz
            % to disregard the lower corner points of 1phase
            if (zz == granularity && yy == 0 || zz == granularity && yy == zz)
                continue;
            end
            yc = yy * step + xoffset / granularity;
            zc = (granularity - zz) * ystep;

            [s, o, w] = toSOW(yc, zc);

            xc = xx;
            
            xslice(islice) = xc;
            yslice(islice) = yc;
            zslice(islice) = zc;

            % HLD-NAC
            HLD = getHLD(ionic, S, MWt, EACN, xx, Cc);
            type = nac(s, o, w, HLD, xi, L);
            
            [phaseslice(islice), colorslice(islice, :)] = getPhase(type);
            typeset(islice) = type;
            i = i + 1;
            islice = islice + 1;
        end
    end
    % Select points that are of the chosen type
    retain = selectPhase(typeset, typeToFind);
    if (numel(retain) < 2)
        continue;
    end
    xslice = xslice(retain);
    yslice = yslice(retain);
    zslice = zslice(retain);
    phaseslice = phaseslice(retain);
    colorslice = colorslice(retain, :);
    typeset = typeset(retain);
    % Select points from convex hull
    retain = boundary(yslice', zslice');
    xslice = xslice(retain);
    yslice = yslice(retain);
    zslice = zslice(retain);
    phaseslice = phaseslice(retain);
    colorslice = colorslice(retain, :);
    typeset = typeset(retain);
    % Append to returned vectors
    insert = lastoffset:(lastoffset+numel(retain)-1);
    x(insert) = xslice;
    y(insert) = yslice;
    z(insert) = zslice;
    colors(insert, :) = colorslice;
    phases(insert) = phaseslice;
    retained(insert) = retain;
    typesetfinal(insert) = typeset;
    lastoffset = lastoffset + numel(retain);
    
end
if (typeToFind == 5)
    save data.mat x y z;
end

    figure(1);
    scatter3(x, y, z, [], colors, 'Filled');
%     figure(2);
%     plot3(x, y, z);