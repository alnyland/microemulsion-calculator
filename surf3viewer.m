% Andrew Nyland, 7/20/21
% Another trial of rendering a volume from 3D data

load data/data4_1.mat

% k = boundary(z', x', y');
k = boundary([x', y', z']);
% k = delaunay(z, x, y);
whichfig = 1;

f = figure;
if (whichfig == 1)
    scatter3(x, y, z, 'Facecolor', 'red','MarkerFaceAlpha',0.7);
    % view(-172, 11);
    view(-10, 16);
    % view(-47, 74);
end
if (whichfig == 2)
    trisurf(k, x, y, z,'Facecolor','red','FaceAlpha',0.7);
    % view(-172, 11);
    view(-10, 16);
    % view(-47, 74);
end

f.Position(3:4) = [400 300];
% pause(10)

% frames = [];
M(numel(80:-0.25:-60)) = struct('cdata',[],'colormap',[]);

axis tight manual;
i = 1;
for az = 80 : -0.25 : -60
    el = 28 - (abs(az)/2.666666667);
    view(az, el);
    drawnow
    M(i) = getframe(gcf);
    i = i + 1;
end

filename = strcat('outVideo', num2str(whichfig), '.mp4');

v = VideoWriter(filename, 'MPEG-4');
open(v);
writeVideo(v, M);
close(v);