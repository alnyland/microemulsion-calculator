% Andrew Nyland - 7/22/21
% Microemulsion Fish Diagram

function fish_diagram()

    [ MWt, Area, S, xi, L, EACN, Cc, T, ionic ] = importMicroemulsionConfig('configTesting.txt');
    granularity = 100;
    
    xTitle = 'wt %';
    yTitle = 'temp';
    
    plot_stuff;
    
    function [x, y, colors, phases] = simulate(granularity, ionic, S, MWt, Area, xi, L, EACN, T, Cc)
%         Configuration
        xGranularity = 200;
        yGranularity = 150;
        xMin = 0;
        xMax = 100;
        yMin = 16; % Temp for now
        yMax = 45;
        xStep = (xMax-xMin)/xGranularity;
        yStep = (yMax-yMin)/yGranularity;
        
        x = [];
        y = [];
        colors = [];
        phases = strings(1, numel(yMin:yStep:yMax)*numel(xMin:xStep:xMax));
        typeset = [];
        
        oil = 0.5;
        water = 0.5;
        
        i = 1; % iterator
        for yy = yMin:yStep:yMax
            for xx = xMin:yStep:xMax
                surfVol = xx/100;
                temp = yy;
                oil = (1 - surfVol) / 2;
                water = oil;
                hld = getHLD(ionic, S, MWt, EACN, temp, Cc);
                type = nac(surfVol, oil, water, hld, xi, L);
                x(i) = xx;
                y(i) = yy;

                [phases(i), colors(i, :)] = getPhase(type);
                typeset(i) = type;
                i = i + 1;
            end
        end
        hld
        colors = typeset;  % for line version
    end % End simulate()
    
    function plot_stuff()
        [x, y, colors, phases] = simulate(granularity, ionic, S, MWt, Area, xi, L, EACN, T, Cc);
        
        % Find lines
        for i = [1 3 4 5]
            retain = selectPhase(colors, i);
            newx = x(retain);
            newy = y(retain);
            retain = boundary(newx', newy');
            
            line(newx(retain), newy(retain));
        end
        xlim([-5, 105]);
        ylim([13, 48]);
        return;
        
        figure(1);
        scatter(x', y', [], colors, 'Filled');
        xlabel(xTitle);
        ylabel(yTitle);
        
        dcm = datacursormode;
        dcm.Enable = 'on';
        dcm.DisplayStyle = 'window';
        dcm.UpdateFcn = {@displayCoordinatesFish, phases, xTitle, yTitle};
    end
end